PropCourier Sans is a proportional version of OSP’s [NotCourier Sans](http://osp.kitchen/foundry/notcouriersans), forked by [Manufactura Independente](http://manufacturaindependente.org).

PropCourier Sans is the ‘official’ typeface of [Libre Graphics Magazine](http://libregraphicsmag.com). The typeface will be gradually improved and tweaked between issues.
The majority of the changes are in the left and right bearings of each glyph. Some glyphs have been changed to fit better into a proportional design (e.g. lowercase i). We have also improved kernings for the most commonly used words such as F/LOSS.

The original NotCourier Sans can be found at the [OSP Foundry](http://osp.kitchen/foundry).